# Frontend Mentor - Rest Countries website

![Preview for the Rest Countries coding challenge](./design/rest-countries.gif)

## Links

- Live site: <https://fem-rest-countries-rouge.vercel.app/>
- Solution page: <https://www.frontendmentor.io/solutions/rest-countries-website-w-reactrouter-rtk-query-headless-ui-swlujeVRrO>

## Notes

WIP 👽👾
