import { Link, useRouteError } from 'react-router-dom'
import styled from 'styled-components'

import Header from '../components/Header'

function Error() {
  // TODO: type?
  const error = useRouteError() as any
  console.error(error)

  return (
    <>
      <Header />
      <Main>
        <ErrorHeader>{error.status}</ErrorHeader>
        <ErrorMessage>{error.statusText}</ErrorMessage>
        <HomeLink to="/">Go to main page</HomeLink>
      </Main>
    </>
  )
}

const Main = styled.main`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 32px;
  min-height: 200px;
`

const ErrorHeader = styled.h2`
  color: var(--color-text);
  font-family: 'Nunito Sans';
  font-weight: 600;
  font-size: 32px;
  margin: 12px;
`

const ErrorMessage = styled.p`
  font-family: 'Nunito Sans';
  color: var(--color-text);
  font-weight: 400;
  font-size: 16px;
  margin: 12px 0 36px;
`

const HomeLink = styled(Link)`
  cursor: pointer;
  margin: 0 0 64px;
  padding: 6px 24px;
  background-color: var(--color-bg-secondary);
  color: var(--color-text);
  border: none;
  border-radius: 2px;
  box-shadow: var(--shadow-back-button);
  font-family: 'Nunito Sans';
  font-weight: 300;
  font-size: 14px;
  text-decoration: none;

  @media ${p => p.theme.LG} {
    margin-bottom: 80px;
    padding: 9px 39px 9px 32px;
    font-size: 16px;
    border-radius: 6px;
  }
`

export default Error
