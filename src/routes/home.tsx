import { memo, useDeferredValue, useMemo } from 'react'
import styled from 'styled-components'
import { useSearchParams } from 'react-router-dom'

import Card from '../components/Card'
import Spinner from '../components/Spinner'
import Filters from '../components/Filters'
import { useGetAllQuery } from '../store/apiSlice'
import { filterCountriesList } from '../utils/filterCountries'
import { formatFetchError } from '../utils/formatFetchError'
import { useSearchParamState } from '../hooks/useSearchParamState'
import { CountryShort } from '../types/types'
import FetchError from '../components/FetchError'

function Home() {
  const { data, isLoading, error } = useGetAllQuery()

  const [searchFilter, setSearchFilter] = useSearchParamState('search', true)
  const [regionFilter, setRegionFilter] = useSearchParamState('region')

  const [, setQuery] = useSearchParams()
  const resetFilters = () => {
    // reset search params directly instead of calling filter setters
    // so browser history updates only once
    setQuery(new URLSearchParams())
  }

  const filteredList = useMemo(
    () => filterCountriesList(searchFilter, regionFilter, data),
    [data, searchFilter, regionFilter]
  )
  const deferredList = useDeferredValue(filteredList)

  const err = formatFetchError(error)

  return (
    <>
      <Filters
        searchFilter={searchFilter}
        setSearchFilter={setSearchFilter}
        regionFilter={regionFilter}
        setRegionFilter={setRegionFilter}
        isStale={deferredList !== filteredList}
      />
      <Cards>
        {err ? (
          <FetchError status={err.status} message={err.message} />
        ) : isLoading || !deferredList ? (
          <Spinner />
        ) : deferredList.length === 0 ? (
          <NotFound>
            <p>Not found</p>
            <button onClick={resetFilters}>Reset filters</button>
          </NotFound>
        ) : (
          <CountryList list={deferredList} data={data!} />
        )}
      </Cards>
    </>
  )
}

const CountryList = memo(
  ({ list, data }: { list: string[]; data: CountryShort[] }) => (
    <>
      {list?.map(el => (
        <Card code={el} data={data!} key={el} />
      ))}
    </>
  )
)

const Cards = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  margin: 32px 16px 30px;

  @media ${p => p.theme.MD} {
    margin-top: 24px;
  }
`

const NotFound = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  p {
    color: var(--color-text);
    font-family: 'Nunito Sans';
    font-weight: 300;
    font-size: 22px;
    margin: 24px 0 36px;

    @media ${p => p.theme.MD} {
      font-size: 28px;
    }
  }

  button {
    cursor: pointer;
    margin: 0 0 64px;
    padding: 6px 22px;
    background-color: var(--color-bg-secondary);
    color: var(--color-text);
    border: none;
    border-radius: 2px;
    box-shadow: var(--shadow-back-button);
    font-family: 'Nunito Sans';
    font-weight: 300;
    font-size: 14px;

    @media ${p => p.theme.LG} {
      margin-bottom: 80px;
      padding: 9px 39px 9px 32px;
      font-size: 16px;
      border-radius: 6px;
    }
  }
`

export default Home
