import { Outlet } from 'react-router-dom'
import styled from 'styled-components'

import Header from '../components/Header'

function Root() {
  return (
    <>
      <Header />
      <Main>
        <Outlet />
      </Main>
    </>
  )
}

const Main = styled.main`
  max-width: 1920px;
  margin: auto;
`

export default Root
