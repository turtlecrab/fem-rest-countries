import { useParams, Link } from 'react-router-dom'
import styled from 'styled-components'

import Spinner from '../components/Spinner'
import { useGetAllQuery, useGetDetailsQuery } from '../store/apiSlice'
import { formatDetails } from '../utils/formatDetails'
import { formatFetchError } from '../utils/formatFetchError'
import BackButton from '../components/BackButton'
import FetchError from '../components/FetchError'

function Details() {
  // TODO: what type here?
  const { code } = useParams<{ code: string }>()

  const { data, isFetching, error } = useGetDetailsQuery(code as string)
  const details = formatDetails(data)

  const { data: allData } = useGetAllQuery()

  const err = formatFetchError(error)

  return (
    // TODO: use grid to get rid of wrappers?
    <Container>
      <BackButton />
      {err ? (
        <FetchError status={err.status} message={err.message} />
      ) : isFetching ? (
        <Spinner />
      ) : (
        <MainWrapper>
          <FlagContainer>
            <FlagImg src={details?.flagUrl} alt={'Flag of ' + details?.name} />
          </FlagContainer>
          <InfoWrapper>
            <Name>{details?.name}</Name>
            <SectionsWrapper>
              <div>
                <Info>
                  <span>Native Name: </span>
                  {Array.from(new Set(details?.nativeNames)).join(', ')}
                </Info>
                <Info>
                  <span>Popuation: </span>
                  {details?.population.toLocaleString()}
                </Info>
                <Info>
                  <span>Region: </span>
                  {details?.region}
                </Info>
                <Info>
                  <span>Sub Region: </span>
                  {details?.subregion}
                </Info>
                <Info>
                  <span>Capital: </span>
                  {details?.capitals.join(', ')}
                </Info>
              </div>
              <div>
                <Info>
                  <span>Top Level Domain: </span>
                  {details?.domains.join(', ')}
                </Info>
                <Info>
                  <span>Currencies: </span>
                  {details?.currencies.join(', ')}
                </Info>
                <Info>
                  <span>Languages: </span>
                  {details?.languages.join(', ')}
                </Info>
              </div>
            </SectionsWrapper>
            <BordersWrapper>
              <BordersHeader>Border Countries: </BordersHeader>
              <BordersList>
                {details?.borders.map(border => (
                  <li key={border}>
                    <BorderLink to={'/' + border}>
                      {allData?.find(el => el.cca3 === border)?.name.common ||
                        border}
                    </BorderLink>
                  </li>
                ))}
              </BordersList>
            </BordersWrapper>
          </InfoWrapper>
        </MainWrapper>
      )}
    </Container>
  )
}

const Container = styled.div`
  padding: 40px 28px;

  @media ${p => p.theme.LG} {
    padding: 80px;
  }
`

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;

  & > * {
    flex: 1;
  }

  @media ${p => p.theme.MD} {
    flex-direction: row;
    gap: 40px;
  }

  @media ${p => p.theme.LG} {
    gap: 120px;
  }
`

const InfoWrapper = styled.div``

const FlagContainer = styled.div``

const FlagImg = styled.img`
  width: 100%;
  display: block;
  margin: 0 0 44px;
`

const Name = styled.h2`
  color: var(--color-text);
  font-family: 'Nunito Sans';
  font-weight: 600;
  font-size: 22px;
  margin: 0 0 23px;

  @media ${p => p.theme.MD} {
    font-size: 32px;
  }

  @media ${p => p.theme.LG} {
    margin: 38px 0 29px;
  }
`

const SectionsWrapper = styled.div`
  display: flex;
  flex-direction: column;

  & > div {
    margin: 0 0 30px;
    flex: 1;

    @media ${p => p.theme.LG} {
      margin-bottom: 60px;
    }
  }

  @media ${p => p.theme.MD} {
    flex-direction: row;
    gap: 20px;
  }
`

const BordersWrapper = styled.div`
  display: flex;
  flex-direction: column;

  @media ${p => p.theme.LG} {
    flex-direction: row;
  }
`

const Info = styled.p`
  color: var(--color-text);
  font-family: 'Nunito Sans';
  font-weight: 300;
  font-size: 14px;
  margin: 0 0 13px;

  & > span {
    font-weight: 600;
  }

  @media ${p => p.theme.MD} {
    font-size: 16px;
  }
`

const BordersHeader = styled.h3`
  font-family: 'Nunito Sans';
  color: var(--color-text);
  font-weight: 600;
  font-size: 16px;
  margin: 0 15px 17px 0;
`

const BordersList = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  display: flex;
  flex-wrap: wrap;
`

const BorderLink = styled(Link)`
  display: inline-block;
  min-width: 96px;
  color: var(--color-text);
  background-color: var(--color-bg-secondary);
  text-decoration: none;
  font-family: 'Nunito Sans';
  font-weight: 300;
  font-size: 12px;
  padding: 6px 12px;
  border-radius: 2px;
  box-shadow: var(--shadow-country-link);
  text-align: center;
  margin: 0 10px 10px 0;
`

export default Details
