import { Link } from 'react-router-dom'
import styled from 'styled-components'

import ThemeSwitchButton from './ThemeSwitchButton'

function Header() {
  return (
    <Container>
      <Wrapper>
        <Heading>
          <LogoLink to="/">Where in the world?</LogoLink>
        </Heading>
        <ThemeSwitchButton />
      </Wrapper>
    </Container>
  )
}

const Container = styled.header`
  min-height: 80px;
  padding: 16px;
  background-color: var(--color-bg-secondary);
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: var(--shadow-header);

  @media ${p => p.theme.MD} {
    padding: 16px 32px;
  }

  @media ${p => p.theme.LG} {
    padding: 16px 80px;
  }
`

// This wrapper is needed so we can set max-width to it
const Wrapper = styled.div`
  flex: 1;
  max-width: 1760px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const Heading = styled.h1`
  margin: 0;
  padding: 0;
  font-family: 'Nunito Sans';
  font-weight: 800;
  font-size: 14px;
  letter-spacing: 0.005em;

  @media ${p => p.theme.MD} {
    font-size: 18px;
  }

  @media ${p => p.theme.LG} {
    font-size: 24px;
  }
`

const LogoLink = styled(Link)`
  color: var(--color-text);
  text-decoration: none;
`

export default Header
