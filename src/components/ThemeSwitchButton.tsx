import styled from 'styled-components'

import { useDarkMode } from '../hooks/useDarkMode'
import { IoMoonSharp } from '@react-icons/all-files/io5/IoMoonSharp'
import { IoMoonOutline } from '@react-icons/all-files/io5/IoMoonOutline'

function ThemeSwitchButton() {
  const [darkMode, toggleDarkMode] = useDarkMode()

  return (
    <Button aria-label="Toggle color mode" onClick={toggleDarkMode}>
      {darkMode ? <IoMoonSharp /> : <IoMoonOutline />} Dark Mode
    </Button>
  )
}

const Button = styled.button`
  cursor: pointer;
  border: none;
  padding: 12px 0;
  color: var(--color-text);
  background-color: transparent;
  font-family: 'Nunito Sans';
  font-size: 12px;
  font-weight: 600;
  display: flex;
  align-items: center;

  @media ${p => p.theme.MD} {
    font-size: 14px;
  }

  @media ${p => p.theme.LG} {
    font-size: 16px;
  }

  & > svg {
    width: 1.125em;
    height: 1.125em;
    margin-right: 0.375em;
  }
`

export default ThemeSwitchButton
