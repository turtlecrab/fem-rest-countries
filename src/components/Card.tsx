import { memo } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { CountryShort } from '../types/types'
import { formatShortDetailsFromList } from '../utils/formatDetails'

interface Props {
  code: string
  data: CountryShort[]
}

function Card({ code, data }: Props) {
  const details = formatShortDetailsFromList(code, data)

  return (
    <CardLink to={'/' + code}>
      <Container>
        <Flag
          src={details.flagUrl}
          alt={'Flag of ' + details.name}
          loading="lazy"
        />
        <Name>{details.name}</Name>
        <Detail>
          <span>Population:</span> {details.population.toLocaleString()}
        </Detail>
        <Detail>
          <span>Region:</span> {details.region}
        </Detail>
        <Detail>
          <span>Capital:</span> {details.capital.join(', ')}
        </Detail>
      </Container>
    </CardLink>
  )
}

const CardLink = styled(Link)`
  text-decoration: none;
  margin: 0 16px 40px;

  @media ${p => p.theme.LG} {
    margin-bottom: 76px;
  }
`

const Container = styled.article`
  width: 264px;
  min-height: 335px;
  border-radius: 6px;
  box-shadow: var(--shadow-card);
  background-color: var(--color-bg-secondary);
  flex: 0 0 264px;
`

const Flag = styled.img`
  width: 100%;
  aspect-ratio: 264/160;
  border-radius: 6px 6px 0 0;
  object-fit: cover;
`

const Name = styled.h2`
  color: var(--color-text);
  margin: 21px 24px 15px;
  font-family: 'Nunito Sans';
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
`

const Detail = styled.p`
  color: var(--color-text);
  margin: 5px 24px;
  font-family: 'Nunito Sans';
  font-weight: 300;
  font-size: 14px;

  & > span {
    font-weight: 600;
  }
`

export default memo(Card)
