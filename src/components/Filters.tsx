import styled, { createGlobalStyle, css, keyframes } from 'styled-components'
import { Listbox, Transition } from '@headlessui/react'
import { HiOutlineSearch } from '@react-icons/all-files/hi/HiOutlineSearch'
import { BiChevronDown } from '@react-icons/all-files/bi/BiChevronDown'
import { Fragment } from 'react'

interface Props {
  searchFilter: string | null
  setSearchFilter: (query: string) => void
  regionFilter: string | null
  setRegionFilter: (region: string | null) => void
  isStale?: boolean
}

function Filters({
  searchFilter,
  setSearchFilter,
  regionFilter,
  setRegionFilter,
  isStale = false,
}: Props) {
  return (
    <Container>
      <InputWrapper>
        <SearchIcon $animate={isStale}>
          <HiOutlineSearch size={20} />
        </SearchIcon>
        <Input
          placeholder="Search for a country..."
          aria-label="Search for a country"
          autoComplete="off"
          value={searchFilter || ''}
          onChange={e => setSearchFilter(e.currentTarget.value)}
        />
        {searchFilter && (
          <ClearButton
            onClick={() => setSearchFilter('')}
            aria-label="Clear search filter"
          >
            &times;
          </ClearButton>
        )}
      </InputWrapper>
      <SelectWrapper>
        <Listbox
          value={regionFilter}
          onChange={value => setRegionFilter(value)}
        >
          <RegionFilterButton>
            {regionFilter || 'Filter by Region'}
          </RegionFilterButton>
          {regionFilter ? (
            <ClearButton
              onClick={() => setRegionFilter('')}
              aria-label="Clear region filter"
            >
              &times;
            </ClearButton>
          ) : (
            <ChevronIcon>
              <BiChevronDown />
            </ChevronIcon>
          )}
          <ListTransitionGlobalStyles />
          <Transition
            as={Fragment}
            enter="listTransition"
            enterFrom="listHiddden"
            enterTo="listShow"
            leave="listTransition"
            leaveFrom="listShow"
            leaveTo="listHiddden"
          >
            <List>
              {['Africa', 'America', 'Asia', 'Europe', 'Oceania'].map(
                region => (
                  <Listbox.Option value={region} key={region}>
                    {({ active, selected }) => (
                      <Option $active={active} $selected={selected}>
                        {region}
                      </Option>
                    )}
                  </Listbox.Option>
                )
              )}
            </List>
          </Transition>
        </Listbox>
      </SelectWrapper>
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  gap: 40px;
  justify-content: space-between;
  margin: 24px 16px;

  @media ${p => p.theme.MD} {
    margin: 24px 32px;
    flex-direction: row;
  }

  @media ${p => p.theme.LG} {
    margin: 48px 80px;
  }
`

const InputWrapper = styled.div`
  position: relative;
`

const staleAnim = keyframes`
  0% {
    opacity: 1;
  }
  25% {
    opacity: 0.2;
  }
  75% {
    opacity: 1;
  }
  100% {
    opacity: 1;
  }
`

const SearchIcon = styled.div<{ $animate: boolean }>`
  position: absolute;
  pointer-events: none;
  top: 0;
  left: 0;
  bottom: 0;
  margin-left: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: var(--color-text);

  ${p =>
    p.$animate &&
    css`
      animation-name: ${staleAnim};
      animation-duration: 0.7s;
      animation-iteration-count: infinite;
      animation-delay: 0.2s;
    `}
`

const Input = styled.input`
  font-family: 'Nunito Sans';
  font-weight: 300;
  font-size: 12px;
  padding-left: 74px;
  padding-right: 48px;
  border: none;
  border-radius: 6px;
  background-color: var(--color-bg-secondary);
  color: var(--color-text);
  box-shadow: var(--shadow-card);

  &::placeholder {
    color: var(--color-text);
  }

  height: 48px;
  width: 100%;

  @media ${p => p.theme.MD} {
    font-size: 14px;
    height: 56px;
    width: 360px;
  }

  @media ${p => p.theme.LG} {
    width: 480px;
  }
`

const SelectWrapper = styled.div`
  position: relative;
  display: flex;
  align-self: flex-start;
`

const RegionFilterButton = styled(Listbox.Button)`
  cursor: pointer;
  font-family: 'Nunito Sans';
  font-weight: 300;
  font-size: 12px;
  padding-left: 24px;
  text-align: left;
  border: none;
  border-radius: 6px;
  background-color: var(--color-bg-secondary);
  color: var(--color-text);
  box-shadow: var(--shadow-card);

  height: 48px;
  width: 200px;

  @media ${p => p.theme.MD} {
    font-size: 14px;
    height: 56px;
  }
`

const ClearButton = styled.button`
  cursor: pointer;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  margin: 12px;
  width: 32px;
  background-color: transparent;
  color: var(--color-text);
  font-size: 24px;
  border: none;
  border-radius: 6px;
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    background-color: var(--color-bg-primary);
  }
`

const ChevronIcon = styled.div`
  pointer-events: none;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  margin: 12px;
  width: 32px;
  background-color: transparent;
  color: var(--color-text);
  font-size: 24px;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
`

const ListTransitionGlobalStyles = createGlobalStyle`
  .listTransition {
    transition: opacity 120ms, transform 120ms;
  }

  .listHiddden {
    opacity: 0;
    transform: scale(0.9);
  }
  
  .listShow {
    opacity: 1;
    transform: scale(1);
  }
`

const List = styled(Listbox.Options)`
  list-style: none;
  position: absolute;
  padding: 4px;
  margin: 0;
  width: 100%;
  top: 60px;
  border-radius: 6px;
  background-color: var(--color-bg-secondary);
  color: var(--color-text);
  box-shadow: var(--shadow-card);
  display: flex;
  flex-direction: column;
  gap: 4px;
`

const Option = styled.div<{ $active?: boolean; $selected?: boolean }>`
  cursor: pointer;
  font-family: 'Nunito Sans';
  font-weight: 300;
  font-size: 12px;
  color: var(--color-text);
  padding: 10px 20px;
  border-radius: 6px;

  background-color: ${p =>
    p.$active
      ? 'var(--color-bg-primary)'
      : p.$selected
      ? 'var(--color-bg-primary)'
      : 'var(--color-bg-secondary)'};

  @media ${p => p.theme.MD} {
    font-size: 14px;
  }
`

export default Filters
