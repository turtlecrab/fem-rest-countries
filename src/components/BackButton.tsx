import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'

function Component() {
  const navigate = useNavigate()

  return (
    <Button onClick={() => navigate(-1)}>
      <svg
        width="19"
        height="13"
        viewBox="0 0 19 13"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M19 6.00623V8.1675H5.10626L8.61022 11.6715L7.082 13.1997L0.968994 7.08676L7.082 0.97374L8.61022 2.50199L5.10601 6.00623H19Z"
          fill="currentColor"
        />
      </svg>
      Back
    </Button>
  )
}

const Button = styled.button`
  cursor: pointer;
  margin: 0 0 64px;
  padding: 6px 24px;
  background-color: var(--color-bg-secondary);
  color: var(--color-text);
  border: none;
  border-radius: 2px;
  box-shadow: var(--shadow-back-button);
  font-family: 'Nunito Sans';
  font-weight: 300;
  font-size: 14px;
  display: flex;
  align-items: center;
  gap: 12px;

  & > svg {
    width: 16px;

    @media ${p => p.theme.LG} {
      width: 19px;
    }
  }

  @media ${p => p.theme.LG} {
    margin-bottom: 80px;
    padding: 9px 39px 9px 32px;
    font-size: 16px;
    border-radius: 6px;
  }
`

export default Component
