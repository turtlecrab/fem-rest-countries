import styled from 'styled-components'

interface Props {
  status: string
  message: string
}

function FetchError({ status, message }: Props) {
  return (
    <Container>
      <ErrorStatus>{status}</ErrorStatus>
      <ErrorMessage>{message}</ErrorMessage>
    </Container>
  )
}

const Container = styled.div`
  padding: 24px;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const ErrorStatus = styled.h2`
  color: var(--color-text);
  font-family: 'Nunito Sans';
  font-weight: 600;
  font-size: 22px;
  margin: 0 0 23px;

  @media ${p => p.theme.MD} {
    font-size: 32px;
  }

  @media ${p => p.theme.LG} {
    margin: 38px 0 29px;
  }
`

const ErrorMessage = styled.p`
  color: var(--color-text);
  font-family: 'Nunito Sans';
  font-weight: 300;
  font-size: 14px;
  margin: 0 0 13px;

  & > span {
    font-weight: 600;
  }

  @media ${p => p.theme.MD} {
    font-size: 16px;
  }
`

export default FetchError
