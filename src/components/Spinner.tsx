import styled, { keyframes } from 'styled-components'
import { GiEarthAfricaEurope } from '@react-icons/all-files/gi/GiEarthAfricaEurope'

function Spinner() {
  return (
    <Container>
      <Icon size={48} />
    </Container>
  )
}

const spinAnim = keyframes`
  from {
      transform: rotate(0deg);
  }
  to {
      transform: rotate(360deg);
  }
`

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 200px;
  color: var(--color-text);
`

const Icon = styled(GiEarthAfricaEurope)`
  animation-name: ${spinAnim};
  animation-duration: 1s;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
`

export default Spinner
