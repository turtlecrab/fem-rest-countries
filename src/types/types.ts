export interface CountryShort {
  name: {
    common: string
    official: string
    nativeName: {
      [key: string]: {
        official: string
        common: string
      }
    }
  }
  cca2: string
  cca3: string
  capital: string[]
  altSpellings: string[]
  region: string
  population: number
  translations?: {
    [key: string]: {
      official: string
      common: string
    }
  }
}

export interface CountryLong {
  name: {
    common: string
    official: string
    nativeName: {
      [key: string]: {
        official: string
        common: string
      }
    }
  }
  cca2: string
  cca3: string
  capital: string[]
  altSpellings: string[]
  region: string
  population: number
  subregion: string
  tld: string[]
  currencies: {
    [key: string]: {
      name: string
      symbol: string
    }
  }
  languages: {
    [key: string]: string
  }
  borders: string[]
}

export interface CountryMininal {
  name: {
    common: string
  }
  cca3: string
}
