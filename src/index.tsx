import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import store, { persistor } from './store/store'
import './index.css'
import Root from './routes/root'
import Details from './routes/details'
import Home from './routes/home'
import Error from './routes/error'

const router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
    errorElement: <Error />,
    children: [
      {
        path: '/',
        element: <Home />,
      },
      {
        path: ':code',
        element: <Details />,
      },
    ],
  },
])

const styledTheme = {
  MD: 'screen and (min-width: 768px)',
  LG: 'screen and (min-width: 1024px)',
}

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
root.render(
  <React.StrictMode>
    <PersistGate persistor={persistor} loading={null}>
      <ThemeProvider theme={styledTheme}>
        <Provider store={store}>
          <RouterProvider router={router} />
        </Provider>
      </ThemeProvider>
    </PersistGate>
  </React.StrictMode>
)
