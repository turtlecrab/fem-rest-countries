import { useEffect } from 'react'
import { toggleDarkMode } from '../store/settingsSlice'
import { useAppDispatch, useAppSelector } from '../store/store'

export function useDarkMode(): [boolean, () => void] {
  const darkModeClassName = 'darkMode'

  const darkMode = useAppSelector(state => state.settings.darkMode)
  useEffect(() => {
    if (darkMode) {
      document.body.classList.add(darkModeClassName)
    } else {
      document.body.classList.remove(darkModeClassName)
    }

    return () => document.body.classList.remove(darkModeClassName)
  }, [darkMode])

  const dispatch = useAppDispatch()
  const toggle = () => dispatch(toggleDarkMode())

  return [darkMode, toggle]
}
