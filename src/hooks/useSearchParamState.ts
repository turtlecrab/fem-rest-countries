import { useSearchParams } from 'react-router-dom'

/**
 * Stores state in the url
 * @param name The name of the parameter
 * @param replaceHistoryWhenNotEmpty If true, it will replace browser history when both old and new value are not empty
 * @returns [state, setState]
 */
export function useSearchParamState(
  name: string,
  replaceHistoryWhenNotEmpty: boolean = false
): [string | null, (newValue: string | null) => void] {
  const [query, setQuery] = useSearchParams()

  const paramValue = query.get(name)

  const setParamValue = (newValue: string | null) => {
    if (newValue) {
      query.set(name, newValue)
    } else {
      query.delete(name)
    }
    if (replaceHistoryWhenNotEmpty) {
      setQuery(query, { replace: Boolean(paramValue) && Boolean(newValue) })
    } else {
      setQuery(query)
    }
  }

  return [paramValue, setParamValue]
}
