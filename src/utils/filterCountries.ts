import { CountryShort } from '../types/types'

export function filterCountriesList(
  search: string | null,
  region: string | null,
  data: CountryShort[] | undefined
) {
  const fullList = data?.map(el => el.cca3)
  if (!search) search = ''
  if (search.trim() === '' && region === null) return fullList

  search = search.trim().toLowerCase()

  const includesSearchQuery = (name: string | undefined) =>
    name ? name.toLowerCase().includes(search as string) : false

  return fullList?.filter(code => {
    const countryData = data?.find(el => el.cca3 === code)
    return (
      countryData &&
      (includesSearchQuery(countryData.name.common) ||
        includesSearchQuery(countryData.name.official) ||
        Object.values(countryData.name.nativeName).some(names =>
          Object.values(names).some(includesSearchQuery)
        ) ||
        ('translations' in countryData &&
          Object.values(countryData.translations!).some(names =>
            Object.values(names).some(includesSearchQuery)
          ))) &&
      (!region || countryData.region.includes(region))
    )
  })
}
