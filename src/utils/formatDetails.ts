import { CountryLong, CountryShort } from '../types/types'

export function formatShortDetailsFromList(
  code: string,
  data: CountryShort[]
): {
  name: string
  population: number
  region: string
  capital: string[]
  flagUrl: string
} {
  const countryData = data.find(element => element.cca3 === code)

  return {
    name: countryData?.name.common || 'n/a',
    population: countryData?.population || 0,
    region: countryData?.region || 'n/a',
    capital: countryData?.capital || [],
    flagUrl: `https://flagcdn.com/${
      countryData?.cca2.toLowerCase() || 'kp'
    }.svg`,
  }
}

interface Details {
  cca3: string
  flagUrl: string
  name: string
  nativeNames: string[]
  population: number
  region: string
  subregion: string
  capitals: string[]
  domains: string[]
  currencies: string[]
  languages: string[]
  borders: string[]
}

export function formatDetails(
  countryData: CountryLong | undefined
): Details | null {
  if (!countryData) return null

  return {
    cca3: countryData.cca3,
    flagUrl: `https://flagcdn.com/${countryData.cca2.toLowerCase()}.svg`,
    name: countryData.name.common,
    // TODO: remove identical?
    nativeNames: Object.values(countryData.name.nativeName).map(
      name => name.common
    ),
    population: countryData.population,
    region: countryData.region,
    subregion: countryData.subregion,
    capitals: countryData.capital,
    domains: countryData.tld,
    currencies: Object.values(countryData.currencies).map(cur => cur.name),
    languages: Object.values(countryData.languages),
    borders: countryData.borders,
  }
}
