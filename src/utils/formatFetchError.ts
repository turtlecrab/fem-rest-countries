import type { SerializedError } from '@reduxjs/toolkit'
import type { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query'

export function formatFetchError(
  error: FetchBaseQueryError | SerializedError | undefined
) {
  if (!error) return

  console.error(error)

  let errStatus = ''
  let errMessage = 'Unknown error'

  if ('status' in error) {
    if (error.status === 400) {
      errMessage = 'Bad request'
    } else if (error.status === 'FETCH_ERROR') {
      errMessage = 'Check your internet connection'
    }
    errStatus = String(error.status)
  }
  return {
    status: errStatus,
    message: errMessage,
  }
}
