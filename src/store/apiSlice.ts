import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { CountryLong, CountryShort } from '../types/types'

export const countriesApi = createApi({
  baseQuery: fetchBaseQuery({ baseUrl: 'https://restcountries.com/v3.1/' }),
  endpoints: build => ({
    getAll: build.query<CountryShort[], void>({
      query: () =>
        'all?fields=name,population,region,capital,cca2,cca3,translations',
    }),
    getDetails: build.query<CountryLong, string>({
      query: code =>
        'alpha/' +
        code +
        '?fields=name,population,region,capital,cca2,cca3,' +
        'subregion,tld,currencies,languages,borders',
    }),
  }),
})

export const { useGetAllQuery, useGetDetailsQuery } = countriesApi
