import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface SettingsState {
  darkMode: boolean
}

const initialState: SettingsState = {
  darkMode: false,
}

const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    toggleDarkMode(state, action: PayloadAction<void>) {
      state.darkMode = !state.darkMode
    },
  },
})

export default settingsSlice.reducer
export const { toggleDarkMode } = settingsSlice.actions
